﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using NergyCertificate.Framework;
using NergyCertificate.Framework.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NergyCertificate.Controllers
{
    public class CourseController : Controller
    {
        private readonly NergyCertDbContext _context;
        public CourseController(NergyCertDbContext Context)
        {
            _context = Context;
        }

        //course Index
        public IActionResult Index()
        {

            var course = _context.Courses
                .Include(p => p.Batch)
                .ThenInclude(p => p.User)
                .ToList();
            return View(course);
        }

        //course Create

        [HttpGet]
        public IActionResult Create()
        {
            ViewBag.bacthlist = new SelectList(_context.Batches, "BatchId", "BatchName");

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(Course course)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    
                    _context.Add(course);
                    await _context.SaveChangesAsync();

                    TempData["Success"] = "course is created successfully";
                    return RedirectToAction("Index", "course");
                }
                return View(course);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(course);
            }

        }


        //course Edit


        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }


            var course = await _context.Courses.FindAsync(id);
            if (course == null)
            {
                return NotFound();
            }

            return View(course);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(int id, Course course)
        {
            if (id != course.CourseId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(course);
                    await _context.SaveChangesAsync();
                    TempData["Success"] = "course is updated successfully";
                }

                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    return View(course);
                }
                return RedirectToAction("Index", "course");


            }

            return View(course);
        }


        //course Delete
        public async Task<IActionResult> Delete(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                var course = await _context.Courses.FindAsync(id);

                if (course == null)
                {
                    return NotFound();
                }
                _context.Courses.Remove(course);
                await _context.SaveChangesAsync();
                TempData["Success"] = "Category is deleted successfully";
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Conflict"))
                    TempData["Error"] = "Unable to delete, please make sure that there are no sub-categories in it";
                else
                    TempData["Error"] = "An error has occured while deleting the item";
            }

            return RedirectToAction("Index", "course");
        }
    }

}

