﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NergyCertificate.Framework;
using NergyCertificate.Framework.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NergyCertificate.Controllers
{
    public class StudentController : Controller
    {
        private readonly NergyCertDbContext _context;
        public StudentController(NergyCertDbContext Context)
        {
            _context = Context;
        }

        //Student Index
        public IActionResult Index()
        {

            var students = _context.Students
                .Include(p => p.Course)
                .ThenInclude(p => p.Batch)
                .ThenInclude(p => p.User)
                .ToList();
            return View(students);
        }


        // AddStudentIndex

        public IActionResult AddStudentIndex()
        {


            var batch = _context.Batches
                .Include(p => p.Courses)
                .ToList();
            if (batch == null)
            {
                return NotFound();
            }

            return View(batch);
        }


        //AddStudentForm
        [HttpGet]
        public IActionResult AddStudentForm(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ViewBag.Course_Id = id;

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddStudentForm(Student student)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    _context.Add(student);
                    await _context.SaveChangesAsync();

                    TempData["Success"] = "Student is created successfully";
                    return RedirectToAction("Index", "Student");
                }
                return View(student);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(student);
            }
        }

        //Student Edit


        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }


            var student = await _context.Students.FindAsync(id);
            if (student == null)
            {
                return NotFound();
            }

            return View(student);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(int id, Student student)
        {
            if (id != student.StudentId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(student);
                    await _context.SaveChangesAsync();
                    TempData["Success"] = "Student is updated successfully";
                }

                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    return View(student);
                }
                return RedirectToAction("Index", "Student");


            }

            return View(student);
        }


        //Student Delete
        public async Task<IActionResult> Delete(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                var student = await _context.Students.FindAsync(id);

                if (student == null)
                {
                    return NotFound();
                }
                _context.Students.Remove(student);
                await _context.SaveChangesAsync();
                TempData["Success"] = "Category is deleted successfully";
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Conflict"))
                    TempData["Error"] = "Unable to delete, please make sure that there are no sub-categories in it";
                else
                    TempData["Error"] = "An error has occured while deleting the item";
            }

            return RedirectToAction("Index", "Student");
        }

      

    }

}

