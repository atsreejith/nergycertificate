﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NergyCertificate.Framework;
using NergyCertificate.Framework.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NergyCertificate.Controllers
{
    public class BatchController : Controller
    {
        private readonly NergyCertDbContext _context;
        public BatchController(NergyCertDbContext Context)
        {
            _context = Context;
        }



        //Batch Index
        public IActionResult Index()
        {

            var batches = _context.Batches
                .Include(p => p.User)
                .ToList();
            return View(batches);
        }

        //Batch Create

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(Batch batch)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    batch.UserId = 1;
                    _context.Add(batch);
                    await _context.SaveChangesAsync();

                    TempData["Success"] = "Batch is created successfully";
                    return RedirectToAction("Index", "Batch");
                }
                return View(batch);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(batch);
            }

        }


        //Batch Edit


        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }


            var batch = await _context.Batches.FindAsync(id);
            if (batch == null)
            {
                return NotFound();
            }

            return View(batch);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(int id, Batch batch)
        {
            if (id != batch.BatchId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(batch);
                    await _context.SaveChangesAsync();
                    TempData["Success"] = "Batch is updated successfully";
                }

                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    return View(batch);
                }
                return RedirectToAction("Index", "Batch");


            }

            return View(batch);
        }


        //Batch Delete
        public async Task<IActionResult> Delete(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                var batch = await _context.Batches.FindAsync(id);

                if (batch == null)
                {
                    return NotFound();
                }
                _context.Batches.Remove(batch);
                await _context.SaveChangesAsync();
                TempData["Success"] = "Category is deleted successfully";
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Conflict"))
                    TempData["Error"] = "Unable to delete, please make sure that there are no sub-categories in it";
                else
                    TempData["Error"] = "An error has occured while deleting the item";
            }

            return RedirectToAction("Index","Batch");
        }
    }

}

