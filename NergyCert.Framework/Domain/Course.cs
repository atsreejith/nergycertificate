﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace NergyCertificate.Framework.Domain
{
    [Table("Course")]
   public class Course
    {
        [Key]
        public int CourseId { get; set; }
        public string CourseName { get; set; }
        public string CourseDescription { get; set; }
        [ForeignKey("Batch")]
        public int BatchId { get; set; }

        public  Batch Batch { get; set; }
    }
}
