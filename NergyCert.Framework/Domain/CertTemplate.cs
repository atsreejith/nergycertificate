﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using NergyCertificate.Framework.Domain;

namespace NergyCertificate.Framework.Domain
{
    [Table("CertTemplate")]
    public class CertTemplate
    {
        [Key]
        public int CertTemplateId { get; set; }

        public string desc { get; set; }

        public virtual ICollection<CertificateData> CertificateDatas { get; set; }

    }
}
