﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace NergyCertificate.Framework.Domain
{
    [Table("Batch")]
    public class Batch
    {
        [Key]
        public int BatchId { get; set; }
        public string  BatchName { get; set; }
        public string  BatchDescription { get; set; }
        [ForeignKey("User")]
        public int UserId { get; set; }

        public  User User { get; set; }

        public virtual ICollection<Course> Courses { get; set; }
    }
}
