﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace NergyCertificate.Framework.Domain
{
   [Table("Student")]
    public class Student
    {
        [Key]
        public int StudentId { get; set; }
        public string FirstName{ get; set; }
        public string LastName{ get; set; }

        [ForeignKey("Course")]
        public int CourseId { get; set; }
        public  Course Course { get; set; }




    }
}
