﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace NergyCertificate.Framework.Domain
{
    public class CertificateData
    {
        [Key]
        public int CertificateId { get; set; }

        public int yearofpass { get; set; }
        public DateTime IssuedDate { get; set; }

        [ForeignKey("Batch")]
        public int BatchId { get; set; }

        [ForeignKey("Student")]
        public int StudentId { get; set; }

        [ForeignKey("CertTemplate")]
        public int CertTemplateId { get; set; }

        public  Batch Batch { get; set; }
        public  Student Student { get; set; }
        public  CertTemplate CertTemplate { get; set; }


    }
}
