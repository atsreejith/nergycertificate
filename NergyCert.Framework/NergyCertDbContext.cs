﻿using Microsoft.EntityFrameworkCore;
using System;
using NergyCertificate.Framework.Domain;

namespace NergyCertificate.Framework
{
    public class NergyCertDbContext:DbContext
    {
        public NergyCertDbContext(DbContextOptions options) : base(options)
        {

        }
        public DbSet<User> Users { get; set; }
        public DbSet<Batch> Batches { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<CertificateData> CertificateDatas { get; set; }
        public DbSet<CertTemplate> CertTemplates { get; set; }

    }
}
